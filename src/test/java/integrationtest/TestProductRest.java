/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integrationtest;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matos
 */
public class TestProductRest {
    
    private Client cli;
    private WebTarget target;
    private String baseURL;
    @Before
    public void setUp() {
        this.cli = ClientBuilder.newClient();
        //baseURL = "http://localhost:8080/tqs_generalstore/restapi/product";
        baseURL = "http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/restapi/product/";
        this.target = cli.target(baseURL);
    }
    
    @Test
    public void testFetchAllProducts(){
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        // String payload = responseDelete.readEntity( String.class);
        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Payload is " + allTodos);
        assertFalse(allTodos.isEmpty());
        
        JsonObject firstProduct = allTodos.getJsonObject(0);
        assertTrue(firstProduct.getString("Name").equals("Cacho Fresco"));
        
        JsonObject idProduct = target.path("1").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals(firstProduct, idProduct );
    }
    
    @Test
    public void testFecthProductProviders() {
        Response response = target.path("/providers/1").request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
       
    }
    
    @Test
    public void testAssociateSuplier(){
        Response response = target.path("/1/1/").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(""));
        assertThat(response.getStatus(), CoreMatchers.is(201));
       
        response = target.path("/providers/1").request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        JsonArray allTodos = response.readEntity(JsonArray.class);
        JsonObject firstProvider = allTodos.getJsonObject(0);
        
        String providerBaseUrl;
        providerBaseUrl = "http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/restapi/provider/";
        //providerBaseUrl = "http:localhost:8080/tqs_generalstore/restapi/provider/";
        WebTarget providersTarget = cli.target(providerBaseUrl);
        JsonObject providerAssociated = providersTarget.path("1").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        
        assertEquals(providerAssociated, firstProvider);
    }
    
    @Test
    public void testDeleteAssociatedSuplier(){
        Response responseDelete = target.path("/1/1").request().delete();
        assertThat(responseDelete.getStatus(), CoreMatchers.is(200));
    }
    
    @After
    public void cleanup() {
        cli.close();
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
