/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integrationtest;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matos
 */
public class TestSales {
    
    private Client cli;
    private WebTarget target;
    private String baseURL;
    @Before
    public void setUp() {
        this.cli = ClientBuilder.newClient();
        //baseURL = "http://localhost:8080/tqs_generalstore/restapi/sale";
        baseURL = "http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/restapi/sale/";
        this.target = cli.target(baseURL);
    }
    
    @Test
    public void testfecthSales(){
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        // String payload = responseDelete.readEntity( String.class);
        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Payload is " + allTodos);
        assertFalse(allTodos.isEmpty());
    
    }
    
    @Test
    public void testAddProductToSale(){
        Response responsePost = target.path("/5/1").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(""));
        assertThat(responsePost.getStatus(), CoreMatchers.is(200));
    
        JsonObject sale = target.path("/5").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        JsonArray prods = sale.getJsonArray("Products");
        
        WebTarget prodTraget = cli.target("http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/restapi/product/");
        JsonObject prod = prodTraget.path("/1").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals(prods.get(0), prod);
    }
    
    @Test
    public void testRemoveProductToSale(){
        Response responsePost = target.path("/5/1").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(""));
        assertThat(responsePost.getStatus(), CoreMatchers.is(200));
    }
    @After
    public void tearDown() {
        cli.close();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
