/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integrationtest;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matos
 */
public class TestClientRest {
    private Client cli;
    private WebTarget target;
    private String baseURL;
    @Before
    public void setUp() {
        this.cli = ClientBuilder.newClient();
        //baseURL = "http://localhost:8080/tqs_generalstore/restapi/client";
        baseURL = "http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/restapi/client/";
        this.target = cli.target(baseURL);
    }
    
    @Test
    public void testFetchAllCLients(){
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        // String payload = responseDelete.readEntity( String.class);
        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Payload is " + allTodos);
        assertFalse(allTodos.isEmpty());
        
        JsonObject firstClient = allTodos.getJsonObject(0);
        assertTrue(firstClient.getString("Name").equals("gustavo"));
        
        JsonObject emailClient = target.path("gustavo@ua.pt").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals(firstClient, emailClient );
        
    }
    
    
    @Test
    public void deleteDummy(){
        Response responseDelete = target.path("/dummy@dumb.com").request().delete();
        assertTrue(responseDelete.getStatus() == 200 || responseDelete.getStatus() == 204);
        
    }
    @Test
    public void postADummy() {
        Response responsePost = target.path("/dummy/dummy@dumb.com/x/y/5").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(""));
        System.out.println(responsePost.getStatus());
        assertTrue(responsePost.getStatus() == 201 || responsePost.getStatus() == 204);
        
        
        JsonObject emailClient = target.path("dummy@dumb.com").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        
        assertEquals("dummy", emailClient.getString("Name"));
    }
    
    @After
    public void tearDown() {
        cli.close();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
