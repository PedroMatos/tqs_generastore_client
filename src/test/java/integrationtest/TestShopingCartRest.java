/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integrationtest;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matos
 */
public class TestShopingCartRest {
    
    private Client cli;
    private WebTarget target;
    private String baseURL;
    @Before
    public void setUp() {
        this.cli = ClientBuilder.newClient();
        //aseURL = "http://localhost:8080/tqs_generalstore/restapi/cart";
        baseURL = "http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/restapi/cart/";
        this.target = cli.target(baseURL);
    }

    @Test
    public void testFetchAllProducts(){
        Response response = target.path("/subject@ua.pt/").request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Payload is " + allTodos);
        assertTrue(allTodos.isEmpty() || !allTodos.isEmpty() );
        
    }
    
    @Test
    public void testAddProductToShoppingCart(){
        Response responsePost = target.path("/subject@ua.pt/1").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(""));
        assertThat(responsePost.getStatus(), CoreMatchers.is(201));
        
        JsonArray cart = target.path("subject@ua.pt/").request(MediaType.APPLICATION_JSON).get(JsonArray.class);
        
        assertTrue(!cart.isEmpty());
    }
    
    @Test
    public void testClearProdFromCart(){
        Response responsePost = target.path("/empty/subject@ua.pt/").request().delete();
        assertThat(responsePost.getStatus(), CoreMatchers.is(200));
        
        JsonArray cart = target.path("subject@ua.pt/").request(MediaType.APPLICATION_JSON).get(JsonArray.class);
        
        assertTrue(cart.isEmpty());
    }
    @After
    public void tearDown(){
        cli.close();
    }
    
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
